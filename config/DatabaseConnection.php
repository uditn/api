<?php

require_once "DatabaseConfig.php";

class DatabaseConnection extends DatabaseConfig
{
    private static $conn;

    private function __construct(){
        try{
            self::$conn = new PDO("mysql:host=".self::$servername.";dbname=".self::$database, self::$username, self::$password);
            self::$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }catch(PDOException $e){
            echo "Connection failed: " . $e->getMessage();
        }
    }

    public static function getConnection(){
    
        if(self::$conn == null){
            new DatabaseConnection();
        }
        return self::$conn;
    }
}

?>