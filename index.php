<?php

    require_once "operation/UserOperation.php";

    $path = $_SERVER['REQUEST_METHOD'];
    
    switch($path){
        case "GET":
            UserOperation::readUser();
            break;

        case "POST":
            
            UserOperation::createUser();
            break;

        case "PUT":

            UserOperation::updateUser();
            break;
        
        case "DELETE":

            UserOperation::deleteUser();
            break;

        default:
    
            echo json_encode(
                array("message" => "Invalid Path.")
            );
    }
    
?>