<?php 

// include model files
require_once "model/User.php";

class UserOperation{

    public static function createUser()
    {
        // required headers
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");


        // initialize object
        $user = new User();

        $data = json_decode(file_get_contents("php://input"));

        if(
            !empty($data->first_name) && 
            !empty($data->last_name) && 
            !empty($data->email) && 
            !empty($data->mobile_number) && 
            !empty($data->password)
        ){

            // Sanitize & Binding data into object
            $user->first_name=htmlspecialchars(strip_tags($data->first_name));
            $user->last_name=htmlspecialchars(strip_tags($data->last_name));
            $user->email=htmlspecialchars(strip_tags($data->email));
            $user->mobile_number=htmlspecialchars(strip_tags($data->mobile_number));
            $user->password=htmlspecialchars(strip_tags($data->password));

            
            // create the product
            if($user->create()){
        
                // set response code - 201 created
                http_response_code(201);
        
                // tell the user
                echo json_encode(array("return" => "true", "message" => "record created successfully."));
            }
        
            // if unable to create the product
            else{
        
                // set response code - 503 service unavailable
                http_response_code(503);
        
                // tell the user
                echo json_encode(array("return" => "false", "message" => "Unable to create record."));
            }
        }

        // tell the user data is incomplete
        else{
        
            // set response code - 400 bad request
            http_response_code(400);
        
            // tell the user
            echo json_encode(array("return" => "false", "message" => "Unable to create record. Data is incomplete."));
        }
    }

    public static function readUser()
    {

        // required headers
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");

        // initialize object
        $user = new User();
        $user->email = $_GET['email'];

        // read records will be here

        $stmts = $user->read();
        $num = $stmts[0]->rowCount();

        // check if more than 0 record found
        if($num > 0){
            $users_record = array();
            $users_record["return"] = "true";
            $users_record['records'] = array();

            while($row = $stmts[0]->fetch(PDO::FETCH_ASSOC)){
                
                $skills_row =  $stmts[1]->fetch(PDO::FETCH_ASSOC);
                $skill_names = explode(",", $skills_row['skill_names']);
                
                extract($row);

                $record = array(
                    "id" => $id,
                    "prefix" => $prefix,
                    "first_name" => $first_name,
                    "last_name" => $last_name,
                    "email" => $email,
                    "password" => $password,
                    "mobile_number" => $mobile_number,
                    "gender" => $gender,
                    "state" => $state,
                    "skills" => $skill_names,
                    "age" => $age,
                    "resume_path" => $resume_path,
                    "avatar_path" => $avatar_path,
                    "user_dir" => $user_dir,
                    "created_date_time" => $created_date_time,
                    "last_updated_date_time" => $last_updated_date_time
                );

                array_push($users_record['records'],$record);
            }

            // set response code - 200 OK
            http_response_code(200);
        
            // show records in json format
            echo json_encode($users_record);

        }

        // no products found will be here
        else{


            // set response code - 404 Not found
            http_response_code(404);
        
            // tell the user no products found
            echo json_encode(
                array("return" => "false", "message" => "No records found.")
            );
        }
    }

    public static function updateUser()
    {
        // required headers
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: PUT");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
        
        // initialize object
        $user = new User();

        $data = json_decode(file_get_contents("php://input"));

        if(!empty($data->email)){

            // Set the email to be edit
            $user->email = $data->email;

            // Sanitize & Set the property value
            $user->id = htmlspecialchars(strip_tags($data->id));
            $user->prefix = htmlspecialchars(strip_tags($data->prefix));
            $user->first_name = htmlspecialchars(strip_tags($data->first_name));
            $user->last_name = htmlspecialchars(strip_tags($data->last_name));
            $user->email = htmlspecialchars(strip_tags($data->email));
            $user->mobile_number = htmlspecialchars(strip_tags($data->mobile_number));
            $user->password = htmlspecialchars(strip_tags($data->password));
            $user->gender = htmlspecialchars(strip_tags($data->gender));
            $user->state = htmlspecialchars(strip_tags($data->state));
            $user->skills = $data->skills;
            $user->age = htmlspecialchars(strip_tags($data->age));
            $user->user_dir = htmlspecialchars(strip_tags($data->user_dir));
            $user->resume_path = htmlspecialchars(strip_tags($data->resume_path));
            $user->avatar_path = htmlspecialchars(strip_tags($data->avatar_path));


            // update the record
            if($user->update()){
            
                // set response code - 200 ok
                http_response_code(200);
            
                echo json_encode(array("return" => "true", "message" => "Redcord updated successfully."));
            }
            
            // if unable to update the record
            else{
            
                // set response code - 503 service unavailable
                http_response_code(503);
            
                echo json_encode(array("return" => "false", "message" => "Unable to update record."));
            }
        }else{

            //set response code - 400 Bad request
            http_response_code(400);

            echo json_encode(array("return" => "false", "message" => "Bad request. Data is insufficient."));
        }
        

    }

    public static function deleteUser()
    {
        // required headers
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: DELETE");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
        
        // initialize object
        $user = new User();

        // Reading data from request
        $data = json_decode(file_get_contents("php://input"));

        // Setting the email to be delete
        $user->email = $data->email;


        // delete the product
        if($user->delete()){
        
            // set response code - 200 ok
            http_response_code(200);
        
            // success message
            echo json_encode(array("return" => "true","message" => "Record is deleted."));
        }
        
        // if unable to delete the record
        else{
        
            // set response code - 503 service unavailable
            http_response_code(503);
        
            // Error message
            echo json_encode(array("return" => "false","message" => "Unable to delete the record."));
        }
    }
}


?>