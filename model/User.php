<?php

require_once "config/DatabaseConnection.php";

class User{
   
    public $id = "";
    public $prefix = "";
    public $first_name = "";
    public $last_name = "";
    public $email = "";
    public $password = "";
    public $mobile_number = "";
    public $gender = "";
    public $state = "";
    public $skills = [];
    public $age = "";
    public $user_dir = "";
    public $resume_path = "";
    public $avatar_path = "";

    // Read all records from database.
    public function read()
    {   
        $conn = $stmt_fetch_users = $stmt_fetch_skills = null;
        
        try{
            // Establishing database connection.
            $conn = DatabaseConnection::getConnection();
            if(empty($this->email)){
                
                // Fetch records from users table
                $query = "SELECT * FROM users";
                $stmt_fetch_users = $conn->prepare($query);
                $stmt_fetch_users->execute();

                // Fetch records from skills table
                $query_skills = "SELECT GROUP_CONCAT(skills.name) as skill_names
                                FROM skills JOIN user_skill ON skills.id = user_skill.fk_skill_id 
                                GROUP BY user_skill.fk_user_id";
                $stmt_fetch_skills = $conn->prepare($query_skills);
                $stmt_fetch_skills->execute();

            }else{
                
                // Fetch records from users table
                $query = "SELECT * FROM users WHERE email = ?";
                $stmt_fetch_users = $conn->prepare($query);
                $stmt_fetch_users->execute([$this->email]);
                
                // This statement for getting user_id
                $stmt = $conn->prepare($query);
                $stmt->execute([$this->email]);
                $id = $stmt->fetch()['id'];

                // Fetch records from skills table
                $query_skills = "SELECT GROUP_CONCAT(skills.name) as skill_names
                                 FROM skills JOIN user_skill 
                                 ON skills.id = user_skill.fk_skill_id 
                                 WHERE user_skill.fk_user_id = ?";

                $stmt_fetch_skills = $conn->prepare($query_skills);
                $stmt_fetch_skills->execute([$id]);

            }
        
        }catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage();
        }
        $conn = null;
        
        return [$stmt_fetch_users, $stmt_fetch_skills];
    }


    // Create record on database.
    public function create()
    {
        $status = false;

        try{
            // Establishing database connection.
            $conn = DatabaseConnection::getConnection();
            $query = "INSERT INTO users(first_name, last_name, email, password, mobile_number) 
                      VALUES (:firstname, :lastname, :email, :password, :mobile_number)";
            $stmt = $conn->prepare($query);

        
            $stmt->bindParam(':firstname', $this->first_name);
            $stmt->bindParam(':lastname', $this->last_name);
            $stmt->bindParam(':email', $this->email);
            $stmt->bindParam(':mobile_number', $this->mobile_number);
            $stmt->bindParam(':password', $this->password);
        
            $stmt->execute();
            $create_id = $stmt->rowCount();
            if($create_id > 0){
                $status = true;
            }                     
                           
        }catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage();
        }

        $conn = null;

        return $status;
    }

    // Update record on database.
    public function update()
    {
        $status = false;

        try{
            // Establishing database connection.
            $conn = DatabaseConnection::getConnection();
            $query = "UPDATE users SET prefix=:prefix, first_name=:first_name, last_name=:last_name, password=:password, 
            mobile_number=:mobile_number, gender=:gender, state=:state, age=:age, resume_path=:resume_path, 
            avatar_path=:avatar_path, user_dir=:user_dir WHERE email=:email";

            $stmt = $conn->prepare($query);

        
            $stmt->bindParam(':prefix', $this->prefix);
            $stmt->bindParam(':first_name', $this->first_name);
            $stmt->bindParam(':last_name', $this->last_name);
            $stmt->bindParam(':mobile_number',$this->mobile_number);
            $stmt->bindParam(':password',$this->password);
            $stmt->bindParam(':gender', $this->gender);
            $stmt->bindParam(':state', $this->state);
            $stmt->bindParam(':age', $this->age);
            $stmt->bindParam(':resume_path', $this->resume_path);
            $stmt->bindParam(':avatar_path', $this->avatar_path);
            $stmt->bindParam(':user_dir', $this->user_dir);
            $stmt->bindParam(':email', $this->email);
        
            $stmt->execute();  
            $row_updated = $stmt->rowCount();

            if(count($this->skills) != 0){

                //Storing skills
                $temp_skills = array("PHP"=>1,"Java"=>2,"HTML"=>3,"CSS"=>4,"Bootstrap"=>5,"MYSQL"=>6);
                
                // getting the user_id
                $stmt_get_id = $conn->prepare("SELECT id FROM users where email = ?");
                $stmt_get_id->execute([$this->email]);
                $id = $stmt_get_id->fetch()["id"];
                

                //Deleting the previous skills
                $conn->prepare("DELETE FROM user_skill WHERE fk_user_id = ?")->execute([$id]);
            
                //Adding updated skills                 
                foreach($this->skills as $skill){

                    $query_skills = "INSERT INTO user_skill(fk_user_id,fk_skill_id) VALUES(:user_id,:skill_id)";
                    $stmt_insert_skills = $conn->prepare($query_skills);
                    $stmt_insert_skills->bindParam(':user_id',$id);
                    $stmt_insert_skills->bindParam(':skill_id',$temp_skills[$skill]);
                    $stmt_insert_skills->execute();
                    $row_updated = $stmt_insert_skills->rowCount();
                    
                }

            }
           
            if($row_updated > 0){
                $status = true;
            }                     
                           
        }catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage();
        }

        $conn = null;

        return $status;
    }

    // Delete records from database.
    public function delete()
    {
        $conn = null;
        $status = false;
        try{
            // Establishing database connection.
            $conn = DatabaseConnection::getConnection();
            
            // This will get the id
            $stmt_get_id = $conn->prepare("SELECT id FROM users where email = ?");
            $stmt_get_id->execute([$this->email]);
            $id = $stmt_get_id->fetch()["id"];

            //Deleting user skills from skills table
            $conn->prepare("DELETE FROM user_skill WHERE fk_user_id = ?")->execute([$id]);

            // Deleting the user data from users table
            $query = "DELETE FROM users WHERE email = ?";
            $stmt_dlt_users = $conn->prepare($query);
            $stmt_dlt_users->execute([$this->email]);
            
            if($stmt_dlt_users->rowcount() > 0){
                $status = true;
            }
 
        }catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage();
        }
        $conn = null;
        
        return $status;
    }

}
?>